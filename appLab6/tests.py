from django.test import TestCase, Client
from django.urls import resolve
from .views import home,redirecting
from django.http import HttpRequest
from .models import StatusDB
from .forms import StatusForm

# Create your tests here.

class UnitTestStory6(TestCase):
  def test_homepage_url_is_exist(self):
    response = Client().get('/home/')
    self.assertEqual(response.status_code, 200)

  def test_notexist_url_is_notexist(self):
    response = Client().get('/gaada/')
    self.assertEqual(response.status_code, 404)

  def test_homepage_using_homepage_function(self):
    response = resolve('/home/')
    self.assertEqual(response.func, home)
  
  def test_homepage_using_homepage_template(self):
    response = Client().get('/home/')
    self.assertTemplateUsed(response, 'home.html')

  def test_landing_page_is_completed(self):
    request = HttpRequest()
    response = home(request)
    html_response = response.content.decode('utf8')
    self.assertIn('Halo, apa kabar?', html_response)
  def test_landing_page_title_is_right(self):
    request = HttpRequest()
    response = home(request)
    html_response = response.content.decode('utf8')
    self.assertIn('<title>Story-6 wibias</title>', html_response)
    
  def test_landing_page_using_redirecting_function(self):
    response = resolve('/')
    self.assertEqual(response.func, redirecting)
    
  def test_landing_page_redirecting(self):
    response = Client().get('/')
    self.assertEqual(response.status_code, 302)
    
  def test_landing_page_redirected_to_home(self):
    response = Client().get('/')
    self.assertRedirects(response, '/home/')
    
  def setUp(cls):
    StatusDB.objects.create(status="coba-coba")
    
  def test_if_models_in_database(self):
    modelsObject = StatusDB.objects.create(status="Duh pusing")
    count_Object = StatusDB.objects.all().count()
    self.assertEqual(count_Object, 2)
    
  def test_if_StatusDB_status_is_exist(self):
    modelsObject = StatusDB.objects.get(id=1)
    statusObj = StatusDB._meta.get_field('status').verbose_name
    self.assertEqual(statusObj, 'status')

  def test_forms_input_html(self):
    form = StatusForm()
    self.assertIn('id="id_status', form.as_p())

  def test_forms_validation_blank(self):
    form = StatusForm(data={'status':''})
    self.assertFalse(form.is_valid())
    self.assertEquals(form.errors['status'], ["This field is required."])

  def test_forms_in_template(self):
    request = HttpRequest()
    response = home(request)
    html_response = response.content.decode('utf8')
    self.assertIn('<form method="POST" class="register-form">', html_response)

