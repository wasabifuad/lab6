from django.shortcuts import render, redirect
from .forms import StatusForm, StatusDB

# Create your views here.
def home(request):
	if request.method == "POST":
		form = StatusForm(request.POST)
		if form.is_valid():
			data = form.save()
			data.save()
			return redirect('/home/')
	else:
		form = StatusForm()

	statusNow = StatusDB.objects.order_by('-date')
	return render(request, 'home.html', {'form': form, 'status': statusNow})


def redirecting(request):
	return redirect('/home/')


