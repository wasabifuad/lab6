from django import forms
from .models import StatusDB


class StatusForm(forms.ModelForm):
    status = forms.CharField(max_length=300, widget=forms.Textarea)
    
    class Meta:
        model = StatusDB
        fields = ('status',)
