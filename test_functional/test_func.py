from django.test import TestCase, Client
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import time

class FunctionalTestStory6(TestCase):
  def setUp(self):
    self.opt = Options()
    self.opt.add_argument('dns-prefetch-disable')
    self.opt.add_argument('no-sandbox')
    self.opt.add_argument('headless')
    self.opt.add_argument('disable-gpu')
    self.selenium = webdriver.Chrome(options=self.opt, executable_path='./chromedriver')
    super(FunctionalTestStory6, self).setUp()
    
  def tearDown(self):
    self.selenium.quit()
    super(FunctionalTestStory6, self).tearDown()
    
  def test_input_todo(self):
    selenium = self.selenium
    # Opening the link we want to test
    selenium.get('http://127.0.0.1:8000/home/')
    # find the form element
    time.sleep(5)
    hello = selenium.find_element_by_id("id_status")
    hello.click()
    hello.send_keys("test")
    butClick = selenium.find_element_by_css_selector("button")
    butClick.click()
    time.sleep(3)
    self.assertIn("test", selenium.page_source)
    